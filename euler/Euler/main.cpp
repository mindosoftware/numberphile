//
//  main.cpp
//  Euler
//
//  Created by Joachim Neumann on 08/05/16.
//  Copyright © 2016 Joachim Neumann. All rights reserved.
//

#include <iostream>
#include <gmpxx.h>
#include <map>
#include <inttypes.h>
#include <boost/thread/thread.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

using namespace std;
using boost::multiprecision::cpp_dec_float_100;

map <long long, mpz_class> pMap;

long long pentagonalNumber(long long n) {
    return 0.5*(3*n*n-n);
}

long long argumentOfGeneralizedPentagonalNumber(long long n) {
    long long res = (n+1) / 2;
    if (! (n%2)) {
        // uneven
        res = -res;
    }
    return res;
}

long long generalizedPentagonalNumber(long long n) {
    long long argument = argumentOfGeneralizedPentagonalNumber(n);
    long long gpm = pentagonalNumber(argument);
    return gpm;
}

long long signForGeneralizedPentagonalNumber(long long n) {
    // sign of gpm
    long long sign;
    n = argumentOfGeneralizedPentagonalNumber(n);
    if (n<0) n = -n;
    n -= 1;
    if (n%2) {
        sign = -1;
    } else {
        sign = 1;
    }
    return sign;
}

cpp_dec_float_100 pApprox2(long long n) {
    if (n > 2251799813685248) return 0;
    cpp_dec_float_100 res = 2.0*n;
    res /= 3;
    res = sqrt(res);
    res *= boost::math::constants::pi<cpp_dec_float_100>();
    res = exp(res);
    cpp_dec_float_100 temp = 3.0;
    temp = sqrt(temp);
    temp *= 4*n;
    res /= temp;
    return res;
}

mpz_class pApprox (long long n) {
    if (n > 65536) return 0;
    return exp(M_PI*sqrt(2.0*n/3.0)) / (4.0*n*sqrt(3.0));
}

mpz_class p (long long n) {
    bool debug = false;
    mpz_class res = 0;
    
    // already calculated?
    if (pMap.find(n) != pMap.end()) {
        res = pMap[n];
        if (debug) cout << "cached p(" << n << ") = " << res << "\n";
        return res;
    } else if (n == 0) {
        // special case n == 0
        res = 1;
        pMap[0] = res;
        return res;
    } else {
        // calculate with iteration
        res = 0;
        mpz_class s1;
        mpz_class s;
        long long penIndex = 1;
        long long argument = n - generalizedPentagonalNumber(penIndex);
        while (argument >= 0) {
            long long s2 = signForGeneralizedPentagonalNumber(penIndex);
            s1 = p(argument);
            mpz_mul_si(s.get_mpz_t(), s1.get_mpz_t(), s2);
            mpz_add(res.get_mpz_t(), res.get_mpz_t(), s.get_mpz_t());
            argument = n - generalizedPentagonalNumber(++penIndex);
        }
        if (n < 300000) pMap[n] = res;
        return res;
    }
}

void calc() {
    long long N = 2000;
    mpz_class x = p(N);
    long long even = 0;
    long long even3 = 0;
    long long even5 = 0;
    for (long long i = 1; i < N; i++) {
//        cout << "      p (" << i << ") = " << pMap[i] << "\n";
        if (pMap[i] % 2 == 0) even ++;
        if (pMap[i] % 3 == 0) even3 ++;
        if (pMap[i] % 5 == 0) even5 ++;
    }
    cout.precision(5);
    cout << "  even2 = " << even   << " " <<   even / (1.0*N) << endl;
    cout << "  even3 = " << even   << " " <<   even3 / (1.0*N) << endl;
    cout << "  even5 = " << even   << " " <<   even5 / (1.0*N) << endl;
    
//    long long N = 2000;
//    mpz_class x = p(N);
//    long long even = 0;
//    long long even3 = 0;
//    long long even5 = 0;
//    for (long long i = 1; i < N; i++) {
////        cout << "      p (" << i << ") = " << pMap[i] << "\n";
//        if (pMap[i] % 2 == 0) even ++;
//        if (pMap[i] % 3 == 0) even3 ++;
//        if (pMap[i] % 5 == 0) even5 ++;
//    }
//    cout.precision(5);
//    cout << "  even2 = " << even   << " " <<   even / (1.0*N) << endl;
//    cout << "  even3 = " << even   << " " <<   even3 / (1.0*N) << endl;
//    cout << "  even5 = " << even   << " " <<   even5 / (1.0*N) << endl;
//    for (long long index = 1; index < 100; index++) {
//    i = 13*13*13*13*1*27371; cout << "      p (" << i << ") = " << p(i) << "\n";
//    i =               27371; cout << "      p (" << i << ") = " << p(i) << "\n";
//    i = 13*13*13*13*1+27371; cout << "      p (" << i << ") = " << p(i) << "\n";
//    i = 13*13*13*13*2+27371; cout << "      p (" << i << ") = " << p(i) << "\n";
//    for (long long i = 10; i < 60; i+= 10) {
//        cout << "      p (" << i << ") = " << p(i) << "\n";
//        cout << "pApprox (" << i << ") = " << pApprox(i) << "\n";
//        cout.precision(std::numeric_limits<cpp_dec_float_100>::digits10);
//        cout << "pApprox2(" << i << ") = " << pApprox2(i) << "\n";
//    }
//    i = 1;
//    for (long long index = 1; index < 100; index++) {
//        cout << "      p (" << i << ") = " << p(i) << "\n";
//        if (i < 70000) cout << "pApprox (" << i << ") = " << pApprox(i) << "\n";
//        cout.precision(std::numeric_limits<cpp_dec_float_100>::digits10);
//        cout << "pApprox2(" << i << ") = " << pApprox2(i) << "\n";
//        i *= 2;
//        if (i < 0) break;
//    }
    
    // debugging
    //    for (long long i = 1; i < 10; i++) cout << "pentagonalNumber(" << i << ") = " << pentagonalNumber(i) << "\n";
    //    for (long long i = 1; i < 10; i++) cout << "argumentOfGeneralizedPentagonalNumber(" << i << ") = " << argumentOfGeneralizedPentagonalNumber(i) << "\n";
    //    for (long long i = 1; i < 10; i++) cout << "generalizedPentagonalNumber(" << i << ") = " << generalizedPentagonalNumber(i) << "\n";
    //    for (long long i = 1; i < 10; i++) cout << "signForGeneralizedPentagonalNumber(" << i << ") = " << signForGeneralizedPentagonalNumber(i) << "\n";
}

int main(int argc, const char * argv[]) {
    boost::thread::attributes threadAttributes;
    threadAttributes.set_stack_size(1024*1024*1024); // 1 Gb stack
    boost::thread myThread(threadAttributes, calc);
    myThread.join();
//    char ch;
//    cin.get(ch);
//    return 0;
}
