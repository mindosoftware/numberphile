//
//  ack.h
//  acko
//
//  Created by Joachim Neumann on 05/05/16.
//  Copyright © 2016 Joachim Neumann. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ack : NSObject

@property (strong, nonatomic) NSMutableDictionary* dict;
@property (strong, nonatomic) NSMutableDictionary* dict2;
@property (strong, nonatomic) NSNumber* mNumber;
@property (strong, nonatomic) NSNumber* nNumber;

@property (nonatomic, strong) NSThread *thread;

- (long long) calcAck: (long long) m : (long long) n;
- (void) calc;

@end
