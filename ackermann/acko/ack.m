//
//  ack.m
//  acko
//
//  Created by Joachim Neumann on 05/05/16.
//  Copyright © 2016 Joachim Neumann. All rights reserved.
//

#import "ack.h"

#define N 10000000
@implementation ack

- (long long) calcAck: (long long) m : (long long) n {
    _mNumber = [NSNumber numberWithLongLong:m];
    _nNumber = [NSNumber numberWithLongLong:n];
    _dict2 = _dict[_mNumber];
    if (!_dict2) {
        if (m < N) {
            _dict[_mNumber] = [[NSMutableDictionary alloc] initWithCapacity:N];
            _dict2 = [_dict objectForKey:_mNumber];
        }
    }
    
    if (_dict2) {
        NSNumber* res = _dict2[_nNumber];
        if (res) {
            //        printf("cashed (%lli, %lli) = %lli\n", m, n, [res longLongValue]);
            return [res longLongValue];
        }
    }
    
    // if we are still here, the dictoinary exists, but
    // the number has not yet been calculated
    long long ans;
    if (m == 0) ans = n + 1;
    else if (n == 0) ans = [self calcAck: m-1: 1];
    else ans = [self calcAck: m-1: [self calcAck: m: n-1] ];
//    printf("calculated (%lli, %lli) = %lli\n", m, n, ans);
    if (m < N) {
        _mNumber = [NSNumber numberWithLongLong:m];
        _nNumber = [NSNumber numberWithLongLong:n];
        _dict2 = _dict[_mNumber];
        _dict2[_nNumber] = [NSNumber numberWithLongLong:ans];
    }
    return ans;
}


- (NSThread *) thread
{
    if (!_thread) {
        _thread = [[NSThread alloc]
                   initWithTarget:self
                   selector:@selector(longloop:)
                   object:nil];
    }
    return _thread;
}

- (void)longloop:(NSNumber *)thenumberhere
{

    _dict = [[NSMutableDictionary alloc] initWithCapacity:N];
    for (long long m = 0; m < 5; m++)
        for (long long n=0; n < 5; n++)
            printf("Ack(%lli, %lli) = %lli\n", m, n, [self calcAck: m: n]);
    printf("DONE");
}

- (void) calc {
    _thread = [[NSThread alloc]
               initWithTarget:self
               selector:@selector(longloop:)
               object:nil];
    [_thread setStackSize:4096*250*1000];
    [self.thread start];
    while(YES) {};
}

@end
