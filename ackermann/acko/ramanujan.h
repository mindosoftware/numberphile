//
//  ramanujan.h
//  acko
//
//  Created by Joachim Neumann on 07/05/16.
//  Copyright © 2016 Joachim Neumann. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ramanujan : NSObject
- (void) calc;
- (float) root: (int) n;

@end
