//
//  ramanujan.m
//  acko
//
//  Created by Joachim Neumann on 07/05/16.
//  Copyright © 2016 Joachim Neumann. All rights reserved.
//

#import "ramanujan.h"

@implementation ramanujan

int N = 10;

- (float) root: (int) n {
    if (n == N+1) {
        return 1;
    }
    float r = [self root: n+1];
    float x = 1 + n * sqrt(r);
    printf("%3i: x = %.4f\n", n, x-1);
    return x;
}

- (void) calc {
    
    // for loop
    float x = N;
    for (int n = N; n > 1; n--) {
        x = (n-1) * sqrt(1.0f + x);
        printf("%3i: x = %.4f\n", n, x);
    }
    
    printf("\n\n");

    
    // recursive
    float xx = sqrt([self root:2]);
    printf("%.4f\n", xx);
}



@end
