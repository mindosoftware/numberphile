//
//  main.m
//  acko
//
//  Created by Joachim Neumann on 05/05/16.
//  Copyright © 2016 Joachim Neumann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ack.h"
#import "ramanujan.h"



int main(int argc, const char * argv[]) {
    @autoreleasepool {
//        ack* ackObject = [[ack alloc] init];
//        [ackObject calc];
        ramanujan* r = [[ramanujan alloc] init];
        [r calc];
    }
    return 0;
}
