//
//  GmpDouble.mm
//  Calculator
//
//  Created by Joachim Neumann on 23/05/16.
//  Copyright © 2016 VISAMED IT. All rights reserved.
//


#include <mpfr.h>
#import "GmpDouble.h"

#define NUMBER_OF_BITS 500

@interface GmpDouble () {
    mpfr_t mpfr;
}

@end


@implementation GmpDouble


- (instancetype) init {
    return [self initWithDouble:0.0];
}

- (instancetype) initWithDouble:(double) d {
    self = [super init];
    if(self != nil) {
        mpfr_init2 (mpfr, NUMBER_OF_BITS);
        [self fromDouble:d];
    }
    return self;
}

- (void) fromDouble: (double) d {
    mpfr_set_d (mpfr, d, MPFR_RNDD);
}

- (void)dealloc
{
    mpfr_clear(mpfr);
}

- (void) pi {
    mpfr_const_pi(mpfr, GMP_RNDD);
}
- (void) e {
    mpfr_t one;
    mpfr_init2 (one, NUMBER_OF_BITS);
    mpfr_set_d (one, 1.0, MPFR_RNDD);
    mpfr_exp(mpfr, one, GMP_RNDD);
    mpfr_clear(one);
}

- (void) add:(GmpDouble*) other {
    mpfr_add(mpfr, mpfr, other->mpfr, GMP_RNDD);
}

- (void) div:(GmpDouble*) other {
    mpfr_div(mpfr, mpfr, other->mpfr, GMP_RNDD);
}

- (void) sub:(GmpDouble*) other {
    mpfr_sub(mpfr, mpfr, other->mpfr, GMP_RNDD);
}

- (void) mul:(GmpDouble*) other {
    mpfr_mul(mpfr, mpfr, other->mpfr, GMP_RNDD);
}

- (void) changeSign {
    mpfr_neg(mpfr, mpfr, GMP_RNDD);
}

- (void) sqrt {
    mpfr_sqrt(mpfr, mpfr, GMP_RNDD);
}
- (void) sqrt3 {
    mpfr_t oneThird;
    mpfr_init2 (oneThird, NUMBER_OF_BITS);
    mpfr_set_d (oneThird, 1.0/3.0, MPFR_RNDD);
    mpfr_pow(mpfr, mpfr, oneThird, GMP_RNDD);
    mpfr_clear(oneThird);
}
- (void) rez {
    mpfr_ui_div(mpfr, 1, mpfr, GMP_RNDD);
}
- (void) fac {
    long n = mpfr_get_si(mpfr, GMP_RNDD);
    if (n >= 0) {
        mpfr_fac_ui(mpfr, n, GMP_RNDD);
    } else {
        mpfr_set_d(mpfr, 0.0, GMP_RNDD);
    }
}
- (void) sin {
    mpfr_sin(mpfr, mpfr, GMP_RNDD);
}
- (void) cos {
    mpfr_cos(mpfr, mpfr, GMP_RNDD);
}
- (void) tan {
    mpfr_tan(mpfr, mpfr, GMP_RNDD);
}
- (void) pow_x__2 {
    mpfr_sqr(mpfr, mpfr, GMP_RNDD);
}
- (void) pow_x__3 {
    mpfr_ui_pow(mpfr, 3, mpfr, GMP_RNDD);
}
- (void) pow_e__x {
    mpfr_exp(mpfr, mpfr, GMP_RNDD);
}
- (void) pow_10__x {
    mpfr_exp10(mpfr, mpfr, GMP_RNDD);
}

- (bool) isNull {
    return mpfr_cmp_d(mpfr, 0.0) == 0;
}

- (bool) isNegative {
    return mpfr_cmp_d(mpfr, 0.0) < 0;
}

- (NSString*) toSimpleString {
    if (mpfr_nan_p(mpfr)) {
        return @"Not a Number";
    }
    if (mpfr_zero_p(mpfr)) {
        // set negative 0 to 0
        mpfr_set_d (mpfr, 0.0, MPFR_RNDD);
    }
    if (mpfr_inf_p(mpfr)) {
        return @"Infinity";
    }
    return [NSString stringWithFormat:@"%g", mpfr_get_d(mpfr, GMP_RNDD)];
}

- (NSString*) debugDescription {
    return [self toDetailString];
}


- (NSString*) toDetailString {
    if (mpfr_nan_p(mpfr)) {
        return @"Not a Number";
    }
    if (mpfr_zero_p(mpfr)) {
        // set negative 0 to 0
        mpfr_set_d (mpfr, 0.0, MPFR_RNDD);
    }
    if (mpfr_inf_p(mpfr)) {
        return @"Infinity";
    }
    mpfr_exp_t expptr;
    char c[1000];
    mpfr_get_str(c, &expptr, 10, 90, mpfr, MPFR_RNDN);
    NSMutableString *s1 = [[NSString stringWithCString:c encoding:[NSString defaultCStringEncoding]] mutableCopy];
    while ([s1 length] < 2) s1 = [[s1 stringByAppendingString:@"0"] mutableCopy];
    if ([s1 hasPrefix:@"-"]) {
        [s1 insertString:@"." atIndex:2];
    } else {
        [s1 insertString:@"." atIndex:1];
    }
    NSString* ret = [NSString stringWithFormat:@"%@ E%02ld", s1, expptr-1];
    return ret;
}

@end
