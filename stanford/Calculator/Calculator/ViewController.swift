//
//  ViewController.swift
//  Calculator
//
//  Created by Joachim Neumann on 09/05/2016.
//  Copyright © 2016 VISAMED IT. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    @IBOutlet private weak var display: UILabel!
    @IBOutlet weak var displayDescription: UILabel!
    @IBOutlet weak var scienceStack: UIStackView!
    @IBOutlet weak var scienceStackWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var keysStack: UIStackView!
    @IBOutlet weak var keysStackWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var displayLabel: UIView!
    @IBOutlet weak var displayHeightConstraint: NSLayoutConstraint!
    private var userIsInTheMiddleOfTypeing = false
    private let fmt = NSNumberFormatter()
    private var brain = CalculatorBrain()
    private var screenWidth:CGFloat = 300.0
    private var screenHeight:CGFloat = 300.0

    private var displayMode: DisplayMode = .simple
    private var displayValue = Gmp(0.0)
    
    
    private var savedProgram: CalculatorBrain.PropertyList?
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.All
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        switch UIDevice.currentDevice().orientation {
        case .LandscapeLeft, .LandscapeRight:
            landscape()
        default:
            portrait()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        fmt.usesSignificantDigits = false
        fmt.maximumSignificantDigits = 10
        switch UIDevice.currentDevice().orientation {
        case .LandscapeLeft, .LandscapeRight:
            screenWidth = view.frame.size.width
            screenHeight = view.frame.size.height
        default:
            screenWidth = view.frame.size.width
            screenHeight = view.frame.size.height
        }
        if (screenWidth > screenHeight) {
            let temp:CGFloat = screenWidth
            screenWidth = screenHeight
            screenHeight = temp
        }
        
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.tapResponse(_:)))
        tapGesture.numberOfTapsRequired = 1
        display.userInteractionEnabled =  true
        display.addGestureRecognizer(tapGesture)
        displayDescription.hidden = true
    }
    
    func updateDisplay() {
        switch displayMode {
        case .simple:
            display.text = displayValue.toString(.simple)
            displayDescription.text = ""
        case .detail:
            display.text = displayValue.toString(.detail)
            displayDescription.text = brain.description
        }
    }
    
    func tapResponse(recognizer: UITapGestureRecognizer) {
        switch displayMode {
        case .simple:
            displayMode = .detail
        case .detail:
            displayMode = .simple
        }
        updateDisplay()
    }
    
    func landscape() {
        displayHeightConstraint.constant = screenHeight * 0.15
        scienceStack.hidden = false
        keysStackWidthConstraint.constant = screenWidth*0.75
        scienceStackWidthConstraint.constant = screenHeight - screenWidth*0.75-1
    }
    
    func portrait() {
        displayHeightConstraint.constant = screenHeight * 0.25
        scienceStack.hidden = true
        keysStackWidthConstraint.constant = screenWidth
        scienceStackWidthConstraint.constant = screenHeight - screenWidth-1

    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        switch UIDevice.currentDevice().orientation{
        case .LandscapeLeft, .LandscapeRight:
            landscape()
        default:
            portrait()
        }
    }
    
    @IBAction func loadProgram() {
        if savedProgram != nil {
            brain.program = savedProgram!
            displayValue = brain.result
            updateDisplay()
            if displayMode == .detail {
                displayDescription.text = brain.description
            }
        }
    }
    
    @IBAction func saveProgram() {
        savedProgram = brain.program
    }
    
    @IBAction private func touchDigit(sender: UIButton) {
        var digit = sender.currentTitle!
        let currentText = display.text!
        // zeros at the beginning (display is "0") are ignored
        if !(digit == "0" && currentText == "0") {
            if userIsInTheMiddleOfTypeing {
                digit = (digit == "." && currentText.rangeOfString(".") != nil) ? "" : digit
                display.text = currentText + digit
            } else {
                digit = (digit == ".") ? "0." : digit
                display.text = digit
                userIsInTheMiddleOfTypeing = true
            }
        }
        if displayMode == .detail {
            displayDescription.text = brain.description
        }
        displayValue.fromString(display.text!)
    }

    @IBAction private func performOperation(sender: UIButton) {
        if (userIsInTheMiddleOfTypeing) {
            displayValue.fromString(display.text!)
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTypeing = false
        }
        if let mathematicalSymbol = sender.currentTitle {
            if mathematicalSymbol == "AC" {
                brain.reset()
            }
            brain.performOperation(mathematicalSymbol)
        }
        displayValue = brain.result
        updateDisplay()
    }
}
