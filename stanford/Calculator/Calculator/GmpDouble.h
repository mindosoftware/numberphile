//
//  GmpDouble.h
//  Calculator
//
//  Created by Joachim Neumann on 23/05/16.
//  Copyright © 2016 VISAMED IT. All rights reserved.
//

#ifndef GmpDouble_h
#define GmpDouble_h

#import <Foundation/Foundation.h>

@interface GmpDouble : NSObject

- (instancetype) init;
- (instancetype) initWithDouble:(double) d;
- (void) fromDouble: (double) d;

- (void) pi;
- (void) e;
- (void) add:(GmpDouble*) other;
- (void) div:(GmpDouble*) other;
- (void) sub:(GmpDouble*) other;
- (void) mul:(GmpDouble*) other;
- (void) sqrt;
- (void) sqrt3;
- (void) rez;
- (void) fac;
- (void) sin;
- (void) cos;
- (void) tan;
- (void) pow_x__2;
- (void) pow_x__3;
- (void) pow_e__x;
- (void) pow_10__x;
- (void) changeSign;
- (bool) isNull;
- (bool) isNegative;
- (NSString*) toSimpleString;
- (NSString*) toDetailString;
- (NSString*) debugDescription;


@end

#endif /* GmpDouble_h */
