//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Joachim Neumann on 09/05/2016.
//  Copyright © 2016 VISAMED IT. All rights reserved.
//

import Foundation

private var internalProgram = [AnyObject]()

class CalculatorBrain {
    
    init() {
        accumulator = Gmp(0.0)
    }
    private var accumulator: Gmp
    
    var isPending: Bool = false
    
    func setOperand(operand: Gmp) {
        internalProgram.append(operand)
        accumulator = operand
    }
    
    func reset() {
        internalProgram.removeAll()
        accumulator.fromDouble(0.0)
        isPending = false
    }
    
    typealias PropertyList = AnyObject
    var program: PropertyList {
        get {
            return internalProgram
        }
        set {
            reset()
            // Do I need to test if newValue =!= nil ???
            if let steps = newValue as? [AnyObject] {
                for step in steps {
                    if let d = step as? Gmp {
                        setOperand((d))
                    }
                    if let op = step as? String {
                        performOperation(op)
                    }
                }
            }
        }
    }
    

    
    private var operations: Dictionary<String, Operation> = [
        "AC": Operation.Reset,
        "π": Operation.UnaryOperation(pi),
        "e": Operation.UnaryOperation(e),
        "x^2": Operation.UnaryOperation(pow_x__2),
        "x^3": Operation.UnaryOperation(pow_x__3),
        "e^x": Operation.UnaryOperation(pow_e__x),
        "10^x": Operation.UnaryOperation(pow_10__x),
        "1/x": Operation.UnaryOperation(rez),
        "x!": Operation.UnaryOperation(fac),
        "√": Operation.UnaryOperation(sqrt),
        "3√": Operation.UnaryOperation(sqrt3),
        "sin": Operation.UnaryOperation(sin),
        "cos": Operation.UnaryOperation(cos),
        "tan": Operation.UnaryOperation(tan),
        "×": Operation.BinaryOperation(*),
        "+": Operation.BinaryOperation(+),
        "−": Operation.BinaryOperation(-),
        "÷": Operation.BinaryOperation(/),
        "±": Operation.UnaryOperation(changeSign),
        "=": Operation.Equals
    ]
    
    private enum Operation {
        case Constant(Gmp)
        case UnaryOperation((Gmp) -> Gmp)
        case BinaryOperation((Gmp, Gmp) -> (Gmp))
        case Equals
        case Reset
    }
    
    var description: String = ""/*{
        get {
            var s = ""
            for step in internalProgram {
                if let d = step as? Gmp {
                    s += d.toString(DisplayMode.simple)
                }
                if let op = step as? String {
                    s += op
                }
            }
            return s
        }
    }*/

    func performOperation(symbol: String) {
        internalProgram.append(symbol)
        if let operation = operations[symbol] {
            switch operation {
            case .Constant(let value):
                accumulator = value
                isPending = false
            case .UnaryOperation(let f):
                accumulator = f(accumulator)
                isPending = false
            case .BinaryOperation(let f):
                if (pending != nil) {
                    executePendingOperation()
                }
                pending = PendingBinaryOperationInfo(binaryFunction: f, firstOperand: accumulator)
                isPending = true // isPending is set false in executePendingOperation
            case .Equals:
                if (pending != nil) {
                    executePendingOperation()
                } else {
                    // there was no operation pending, delete the program
                    internalProgram.removeAll()
                    internalProgram.append(result)
                }
            case .Reset:
                reset()
            }
        }
    }

    private func executePendingOperation() {
        accumulator = pending!.binaryFunction(pending!.firstOperand, accumulator)
        pending = nil;
        isPending = false
    }
    
    private var pending: PendingBinaryOperationInfo?

    private struct PendingBinaryOperationInfo {
        var binaryFunction: (Gmp, Gmp) -> (Gmp)
        var firstOperand: Gmp
    }
    
    var result: Gmp {
        get {
            return accumulator
        }
    }
}