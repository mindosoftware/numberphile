//
//  Gmp.swift
//  Calculator
//
//  Created by Joachim Neumann on 24/05/16.
//  Copyright © 2016 VISAMED IT. All rights reserved.
//

import Foundation

enum DisplayMode {
    case simple
    case detail
}

func + (left: Gmp, right: Gmp) -> Gmp {
    left.d.add(right.d)
    return left
}
func / (left: Gmp, right: Gmp) -> Gmp {
    left.d.div(right.d)
    return left
}
func - (left: Gmp, right: Gmp) -> Gmp {
    left.d.sub(right.d)
    return left
}
func * (left: Gmp, right: Gmp) -> Gmp {
    left.d.mul(right.d)
    return left
}
func sqrt(left: Gmp) -> Gmp {
    left.d.sqrt()
    return left
}
func sqrt3(left: Gmp) -> Gmp {
    left.d.sqrt3()
    return left
}
func rez(left: Gmp) -> Gmp {
    left.d.rez()
    return left
}
func fac(left: Gmp) -> Gmp {
    left.d.fac()
    return left
}
func sin(left: Gmp) -> Gmp {
    left.d.sin()
    return left
}
func cos(left: Gmp) -> Gmp {
    left.d.cos()
    return left
}
func tan(left: Gmp) -> Gmp {
    left.d.tan()
    return left
}
func changeSign(left: Gmp) -> Gmp {
    left.d.changeSign()
    return left
}
func pi(left: Gmp) -> Gmp {
    left.d.pi()
    return left
}
func e(left: Gmp) -> Gmp {
    left.d.e()
    return left
}
func pow_x__2(left: Gmp) -> Gmp {
    left.d.pow_x__2()
    return left
}
func pow_x__3(left: Gmp) -> Gmp {
    left.d.pow_x__3()
    return left
}
func pow_e__x(left: Gmp) -> Gmp {
    left.d.pow_e__x()
    return left
}
func pow_10__x(left: Gmp) -> Gmp {
    left.d.pow_10__x()
    return left
}


class Gmp {
    private var d: GmpDouble
    
    init (_ dd: Double) {
        d = GmpDouble(double: dd)
    }
    func isNull() -> Bool {
        if d.isNull() {
            return true
        } else {
            return false
        }
    }
    func isNegtive() -> Bool {
        if d.isNegative() {
            return true
        } else {
            return false
        }
    }

    func fromDouble (dd: Double) {
        d = GmpDouble(double: dd)
    }
    func fromString (dd: String) {
        let scientific = dd.stringByReplacingOccurrencesOfString(" E", withString: "E")
        let decimalNumber = NSDecimalNumber(string: scientific)
        
        if decimalNumber == NSDecimalNumber.notANumber() {
            d.fromDouble(0)
        } else {
            d.fromDouble(decimalNumber.doubleValue)
        }
    }
    func toString(displayMode: DisplayMode) -> String {
        switch displayMode {
        case .simple:
                return d.toSimpleString()
        case .detail:
                return d.toDetailString()
        }
    }
}
